import { AutorEntity } from "./autor-entity";
import { CategoriaEntity } from "./categoria-entity";

export class BooksEntity {
    id !: number;
    titulo!: string;
    estado!: boolean;
    precio!: number;
    autor!: AutorEntity;
    categoria !: CategoriaEntity;
}
