import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AutorEntity } from '../entities/autor-entity';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })

};

@Injectable({
  providedIn: 'root'
})
export class AutoresService {

  private refresh: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private apiUrl = 'https://restcountries.com/v3.1/all';

  public getRefresh(): Observable<boolean>{
    return this.refresh.asObservable();
  }

  public setRefresh(value: boolean): void{
    this.refresh.next(value);
  }

  constructor(private http: HttpClient, private router: Router) { }


  public getAllAutores(): Observable<any> {
    return this.http.get(environment.apiUrl+ 'autores', httpOptions).pipe(map(data => data as any));
  }

  public save(datos: AutorEntity) : Observable<AutorEntity>{
    return this.http.post<AutorEntity>(environment.apiUrl+'autores', datos, httpOptions).pipe(map(data=>data as AutorEntity))
  }

  public edit(datos: AutorEntity) : Observable<AutorEntity>{
    return this.http.put<AutorEntity>(environment.apiUrl+'autores/' + datos.id, datos, httpOptions).pipe(map(data=>data as AutorEntity))
  }

  getCountries(): Observable<any> {
    return this.http.get(this.apiUrl);
  }
}
