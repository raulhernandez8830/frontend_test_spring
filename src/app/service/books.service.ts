import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BooksEntity } from '../entities/books-entity';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })

};

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private refresh: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public getRefresh(): Observable<boolean>{
    return this.refresh.asObservable();
  }

  public setRefresh(value: boolean): void{
    this.refresh.next(value);
  }

  constructor(private http: HttpClient, private router: Router) { }


  public getAllBooks(): Observable<any> {
    return this.http.get(environment.apiUrl+ 'libros', httpOptions).pipe(map(data => data as any));
  }

  public save(datos: BooksEntity) : Observable<BooksEntity>{
    return this.http.post<BooksEntity>(environment.apiUrl+'libros', datos, httpOptions).pipe(map(data=>data as BooksEntity))
  }

  public edit(datos: BooksEntity) : Observable<BooksEntity>{
    return this.http.put<BooksEntity>(environment.apiUrl+'libros/' + datos.id, datos, httpOptions).pipe(map(data=>data as BooksEntity))
  }


  public delete(datos: BooksEntity) : Observable<BooksEntity>{
    return this.http.put<BooksEntity>(environment.apiUrl+'libros/' + datos.id, datos, httpOptions).pipe(map(data=>data as BooksEntity))
  }
}
