import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CategoriaEntity } from '../entities/categoria-entity';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })

};

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  private refresh: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public getRefresh(): Observable<boolean>{
    return this.refresh.asObservable();
  }

  public setRefresh(value: boolean): void{
    this.refresh.next(value);
  }

  constructor(private http: HttpClient, private router: Router) { }


  public getAllCategorias(): Observable<any> {
    return this.http.get(environment.apiUrl+ 'categorias', httpOptions).pipe(map(data => data as any));
  }

  public save(datos: CategoriaEntity) : Observable<CategoriaEntity>{
    return this.http.post<CategoriaEntity>(environment.apiUrl+'categorias', datos, httpOptions).pipe(map(data=>data as CategoriaEntity))
  }

  public edit(datos: CategoriaEntity) : Observable<CategoriaEntity>{
    return this.http.put<CategoriaEntity>(environment.apiUrl+'categorias/' + datos.id, datos, httpOptions).pipe(map(data=>data as CategoriaEntity))
  }
}

