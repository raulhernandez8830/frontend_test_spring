import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AutorEntity } from 'src/app/entities/autor-entity';
import { BooksEntity } from 'src/app/entities/books-entity';
import { CategoriaEntity } from 'src/app/entities/categoria-entity';
import { BooksService } from 'src/app/service/books.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  datos: BooksEntity = new BooksEntity();
  accion!:string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,  private msg: MatSnackBar, private books_service: BooksService,
  public dialog: MatDialog) {
   
   }

  ngOnInit(): void {
    this.datos = this.data.book;

    if(this.datos.estado === false){
      this.accion = 'activar';
    }else{
      this.accion = 'inactivar';
    }
  }

  cambiarEstado(){
    let datos_autor : AutorEntity = new AutorEntity();
    datos_autor.id = this.datos.autor.id;

    let datos_categoria : CategoriaEntity = new CategoriaEntity();
    datos_categoria.id = this.datos.categoria.id;
   
    let datos_libro : BooksEntity = new BooksEntity();
    datos_libro.id = this.datos.id;

    if(this.datos.estado === false){
      datos_libro.estado = true;
    }else{
      datos_libro.estado = false;
    }

    
    datos_libro.autor = datos_autor;
    datos_libro.categoria = datos_categoria;
    datos_libro.titulo = this.datos.titulo;
    datos_libro.precio = this.datos.precio;


    this.books_service.delete(datos_libro).subscribe(
      {
        next:(response) => {
          this.msg.open('¡¡ Libro inactivado con éxito!!', 'Ok',{
            duration: 4000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['success_dialog']
          });
          this.books_service.setRefresh(true);
          this.dialog.closeAll();
        },
        error:(err) =>{
          this.msg.open('¡¡ Algo salió mal !!', 'Ok',{
            duration: 4000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['error_dialog']
          })
        }
      }
    );
  }

}
