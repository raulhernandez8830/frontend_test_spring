import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, map, startWith } from 'rxjs';
import { AutorEntity } from 'src/app/entities/autor-entity';
import { BooksEntity } from 'src/app/entities/books-entity';
import { CategoriaEntity } from 'src/app/entities/categoria-entity';
import { AutoresService } from 'src/app/service/autores.service';
import { BooksService } from 'src/app/service/books.service';
import { CategoriasService } from 'src/app/service/categorias.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  form!: FormGroup;
  categorias!: CategoriaEntity[];
  autores!: AutorEntity[];
  autoresOptions!: Observable<AutorEntity[]>;
  autor_seleccionado !: number;


  categoriasOptions!: Observable<CategoriaEntity[]>;
  categoria_seleccionada !: number;

  constructor(private categoria_service: CategoriasService, private autores_service: AutoresService, private msg: MatSnackBar,
    private books_service: BooksService, public dialog: MatDialog) {
    this.form = new FormGroup({
      'titulo' : new FormControl('', [Validators.required]),
      'precio' : new FormControl('', [Validators.required]),
      'autor_id' : new FormControl('', [Validators.required]),
      'categoria_id' : new FormControl('', [Validators.required]),
    });
   }

  ngOnInit(): void {
    this.getAutores();
    this.getCategorias();
    
  }

  private _filter_autores(value: string): AutorEntity[] {
    const filterValue = value.toLowerCase();

    return this.autores.filter(option => 
      option.nombre.toLowerCase().includes(filterValue)
      );
  }

  private _filter_categorias(value: string): CategoriaEntity[] {
    const filterValue = value.toLowerCase();

    return this.categorias.filter(option => 
      option.nombre.toLowerCase().includes(filterValue)
      );
  }

  crear(){
   
    let datos_autor : AutorEntity = new AutorEntity();
    datos_autor.id = this.autor_seleccionado;

    let datos_categoria : CategoriaEntity = new CategoriaEntity();
    datos_categoria.id = this.categoria_seleccionada;
   
    let datos_libro : BooksEntity = new BooksEntity();
    datos_libro.estado = true;
    datos_libro.autor = datos_autor;
    datos_libro.categoria = datos_categoria;
    datos_libro.titulo = this.form.controls["titulo"].value;
    datos_libro.precio = this.form.controls["precio"].value;


    this.books_service.save(datos_libro).subscribe(
      {
        next:(response) => {
          this.msg.open('¡¡ Libro creado con éxito!!', 'Ok',{
            duration: 4000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['success_dialog']
          });
          this.books_service.setRefresh(true);
          this.dialog.closeAll();
        },
        error:(err) =>{
          this.msg.open('¡¡ Algo salió mal !!', 'Ok',{
            duration: 4000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['error_dialog']
          })
        }
      }
    );
    
  }


  getIdAutor(data: any){
   this.autores.forEach(element=>{
    if(element["nombre"] === data){
      this.autor_seleccionado = element["id"];
    }
   });
  }

  getIdCategoria(data: any){
    this.categorias.forEach(element=>{
     if(element["nombre"] === data){
       this.categoria_seleccionada = element["id"];
     }
    });
   }
  

  getCategorias(){
    this.categoria_service.getAllCategorias().subscribe(
      data=>{
        this.categorias = data;


        this.categoriasOptions = this.form.controls["categoria_id"].valueChanges.pipe(
          startWith(''),
          map(value => this._filter_categorias(value || '')),
        );

      }
    );
  }

  getAutores(){
    this.autores_service.getAllAutores().subscribe(
      data=>{
        this.autores = data;

        this.autoresOptions = this.form.controls["autor_id"].valueChanges.pipe(
          startWith(''),
          map(value => this._filter_autores(value || '')),
        );
      }
    );
  }
}
