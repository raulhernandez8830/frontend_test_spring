import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { BooksService } from 'src/app/service/books.service';
import { DeleteComponent } from '../delete/delete.component';
import { EditComponent } from '../edit/edit.component';
import { CreateComponent } from '../create/create.component';
import { AutorEntity } from 'src/app/entities/autor-entity';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  texto!:string;
  displayedColumns: string[] = ['nombre',  'precio', 'nombre_autor' , 'nombre_categoria', 'estado', 'Acciones'];
  books_data:any = new MatTableDataSource<any>([]);
  @ViewChild('paginator') paginator: MatPaginator | undefined;
 
  
  constructor(public dialog: MatDialog, public _MatPaginatorIntl: MatPaginatorIntl, private router: Router, private books_service: BooksService){}

  ngAfterViewInit() {
    this.books_data.paginator = this.paginator;
  }

  call_book_list(){
    this.books_service.getRefresh().subscribe((value: boolean) =>{
      if(value){
        this.getBooks();
      }
    })
  }

  getBooks(){
    this.books_service.getAllBooks().subscribe(
      data=>{
        this.books_data.data = data;
      },
      err=>{},
      ()=>{
      }
    );
  }


  ngOnInit(): void {
    this.books_service.setRefresh(true);
    this.call_book_list();
    this._MatPaginatorIntl.itemsPerPageLabel = 'Registros a mostrar: ';
  }

  create(){
    this.dialog.open(CreateComponent,{
      width: "55%",
    });
  }

  edit(data:any){
    this.dialog.open(EditComponent,{
      data:{book: data},
      width: "55%",
    });
  }

  delete(data:any){
    this.dialog.open(DeleteComponent,{
      data:{book: data},
      width: "35%",
    });
  }


  filtrar(filterValue :any) {
    this.books_data.filter = filterValue.trim().toLowerCase();
  }

}
