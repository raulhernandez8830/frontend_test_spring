import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminBooksRoutingModule } from './admin-books-routing.module';
import { ViewComponent } from './view/view.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { MatModuleModule } from 'src/app/mat-module.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ViewComponent,
    CreateComponent,
    EditComponent,
    DeleteComponent,
  ],
  imports: [
    CommonModule,
    AdminBooksRoutingModule,
    MatModuleModule,
    FormsModule,
    ReactiveFormsModule 
  ]
})
export class AdminBooksModule { }
