import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelcomeRoutingModule } from './welcome-routing.module';
import { ViewComponent } from './view/view.component';
import { MatModuleModule } from 'src/app/mat-module.module';


@NgModule({
  declarations: [
    ViewComponent
  ],
  imports: [
    CommonModule,
    WelcomeRoutingModule,
    MatModuleModule
  ]
})
export class WelcomeModule { }
