import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewComponent } from './view/view.component';

const routes: Routes = [
  {
    path: '', component: ViewComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./../dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'admin-books',
        loadChildren: () => import('./../admin-books/admin-books.module').then(m => m.AdminBooksModule)
      },
      {
        path: 'welcome',
        loadChildren: () => import('./../welcome/welcome.module').then(m => m.WelcomeModule)
      },
      {
        path: 'autores',
        loadChildren: () => import('./../autores/autores.module').then(m => m.AutoresModule)
      },
      {
        path: 'categorias',
        loadChildren: () => import('./../categorias/categorias.module').then(m => m.CategoriasModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
