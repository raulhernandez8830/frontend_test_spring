import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CategoriaEntity } from 'src/app/entities/categoria-entity';
import { CategoriasService } from 'src/app/service/categorias.service';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  form!: FormGroup;
  constructor(private categoria_service: CategoriasService, @Inject(MAT_DIALOG_DATA) public data: any, private msg: MatSnackBar,
    public dialog: MatDialog) {
    this.form = new FormGroup({
      'nombre' : new FormControl('', [Validators.required]),
      'descripcion' : new FormControl('', [Validators.required]),
    });
   }


  ngOnInit(): void {
  }

  crear(){
    let datos_categoria : CategoriaEntity = new CategoriaEntity();
    datos_categoria = this.form.value;

    this.categoria_service.save(datos_categoria).subscribe(
      {
        next:(response) => {
          this.msg.open('¡¡ Categoría creada con éxito!!', 'Ok',{
            duration: 4000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['success_dialog']
          });
          this.categoria_service.setRefresh(true);
          this.dialog.closeAll();
        },
        error:(err) =>{
          this.msg.open('¡¡ Algo salió mal !!', 'Ok',{
            duration: 4000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['error_dialog']
          })
        }
      }
    );
  }

}
