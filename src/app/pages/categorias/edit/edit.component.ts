import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CategoriaEntity } from 'src/app/entities/categoria-entity';
import { CategoriasService } from 'src/app/service/categorias.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  form!: FormGroup;
  datos_editar: CategoriaEntity = new CategoriaEntity();
  constructor(private categoria_service: CategoriasService, @Inject(MAT_DIALOG_DATA) public data: any, private msg: MatSnackBar,
    public dialog: MatDialog) {
    this.form = new FormGroup({
      'nombre' : new FormControl('', [Validators.required]),
      'descripcion' : new FormControl('', [Validators.required]),
    });
   }


  ngOnInit(): void {
    this.datos_editar = this.data.categoria;

    this.form.patchValue(this.datos_editar)
  }

  editar(){
    let datos_categoria : CategoriaEntity = new CategoriaEntity();
    datos_categoria = this.form.value;
    datos_categoria.id = this.datos_editar.id;

    this.categoria_service.edit(datos_categoria).subscribe(
      {
        next:(response) => {
          this.msg.open('¡¡ Categoría editada con éxito!!', 'Ok',{
            duration: 4000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['success_dialog']
          });
          this.categoria_service.setRefresh(true);
          this.dialog.closeAll();
        },
        error:(err) =>{
          this.msg.open('¡¡ Algo salió mal !!', 'Ok',{
            duration: 4000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['error_dialog']
          })
        }
      }
    );
  }


}
