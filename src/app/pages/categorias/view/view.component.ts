import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoriasService } from 'src/app/service/categorias.service';
import { EditComponent } from '../edit/edit.component';
import { CreateComponent } from '../create/create.component';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  texto!:string;
  displayedColumns: string[] = ['nombre',  'descripcion', 'Acciones'];
  categorias_data:any = new MatTableDataSource<any>([]);
  @ViewChild('paginator') paginator: MatPaginator | undefined;
 
  
  constructor(public dialog: MatDialog, public _MatPaginatorIntl: MatPaginatorIntl, private router: Router, private categorias_service: CategoriasService){}

  ngAfterViewInit() {
    this.categorias_data.paginator = this.paginator;
  }

  call_categorias_list(){
    this.categorias_service.getRefresh().subscribe((value: boolean) =>{
      if(value){
        this.getCategorias();
      }
    })
  }

  getCategorias(){
    this.categorias_service.getAllCategorias().subscribe(
      data=>{
        this.categorias_data.data = data;
      },
      err=>{},
      ()=>{
      }
    );
  }


  ngOnInit(): void {
    this.categorias_service.setRefresh(true);
    this.call_categorias_list();
    this._MatPaginatorIntl.itemsPerPageLabel = 'Registros a mostrar: ';
  }

  create(){
    this.dialog.open(CreateComponent,{
      width: "55%",
    });
  }

  edit(data:any){
    this.dialog.open(EditComponent,{
      data:{categoria: data},
      width: "55%",
    });
  }



  filtrar(filterValue :any) {
    this.categorias_data.filter = filterValue.trim().toLowerCase();
  }
}
