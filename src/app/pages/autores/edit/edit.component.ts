import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, map, startWith } from 'rxjs';
import { AutorEntity } from 'src/app/entities/autor-entity';
import { AutoresService } from 'src/app/service/autores.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  paisesOptions!: Observable<any[]>;
  countries!: any[];
  form!: FormGroup;
  datos_editar: AutorEntity = new AutorEntity();
  constructor(private autores_service: AutoresService, @Inject(MAT_DIALOG_DATA) public data: any, private msg: MatSnackBar,
    public dialog: MatDialog) {
    this.form = new FormGroup({
      'nombre' : new FormControl('', [Validators.required]),
      'fechaNacimiento' : new FormControl('', [Validators.required]),
      'pais' : new FormControl('', [Validators.required]),
    });
   }

   getPaises(){
    this.autores_service.getCountries().subscribe(data => {
      this.countries = data;

      this.paisesOptions = this.form.controls["pais"].valueChanges.pipe(
        startWith(''),
        map(value => this._filter_paises(value || '')),
      );

    });
   }


   private _filter_paises(value: string): AutorEntity[] {
    const filterValue = value.toLowerCase();

    return this.countries.filter(option => 
      option.name.common.toLowerCase().includes(filterValue)
      );
  }

  ngOnInit(): void {
    this.getPaises();
    this.datos_editar = this.data.categoria;
    this.form.patchValue(this.datos_editar)
  }

  editar(){
    let datos_autores : AutorEntity = new AutorEntity();
    datos_autores = this.form.value;
    datos_autores.id = this.datos_editar.id;

    this.autores_service.edit(datos_autores).subscribe(
      {
        next:(response) => {
          this.msg.open('¡¡ Autor editado con éxito!!', 'Ok',{
            duration: 4000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['success_dialog']
          });
          this.autores_service.setRefresh(true);
          this.dialog.closeAll();
        },
        error:(err) =>{
          this.msg.open('¡¡ Algo salió mal !!', 'Ok',{
            duration: 4000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['error_dialog']
          })
        }
      }
    );
  }


}

