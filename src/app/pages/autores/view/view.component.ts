import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { AutoresService } from 'src/app/service/autores.service';
import { CreateComponent } from '../create/create.component';
import { EditComponent } from '../edit/edit.component';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  texto!:string;
  displayedColumns: string[] = ['nombre',  'pais', 'fecha_nacimiento', 'Acciones'];
  autores_data:any = new MatTableDataSource<any>([]);
  @ViewChild('paginator') paginator: MatPaginator | undefined;
 
  
  constructor(public dialog: MatDialog, public _MatPaginatorIntl: MatPaginatorIntl, private router: Router, private autores_service: AutoresService){}

  ngAfterViewInit() {
    this.autores_data.paginator = this.paginator;
  }

  call_categorias_list(){
    this.autores_service.getRefresh().subscribe((value: boolean) =>{
      if(value){
        this.getCategorias();
      }
    })
  }

  getCategorias(){
    this.autores_service.getAllAutores().subscribe(
      data=>{
        this.autores_data.data = data;
      },
      err=>{},
      ()=>{
      }
    );
  }


  ngOnInit(): void {
    this.autores_service.setRefresh(true);
    this.call_categorias_list();
    this._MatPaginatorIntl.itemsPerPageLabel = 'Registros a mostrar: ';
  }

  create(){
    this.dialog.open(CreateComponent,{
      width: "55%",
    });
  }

  edit(data:any){
    this.dialog.open(EditComponent,{
      data:{categoria: data},
      width: "55%",
    });
  }



  filtrar(filterValue :any) {
    this.autores_data.filter = filterValue.trim().toLowerCase();
  }

}
