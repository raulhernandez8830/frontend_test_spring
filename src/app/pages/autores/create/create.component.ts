import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AutorEntity } from 'src/app/entities/autor-entity';
import { AutoresService } from 'src/app/service/autores.service';
import { DateAdapter } from '@angular/material/core';
import { Observable, map, startWith } from 'rxjs';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  countries!: any[];
  form!: FormGroup;
  paisesOptions!: Observable<any[]>;
  
  constructor(private autores_service : AutoresService, @Inject(MAT_DIALOG_DATA) public data: any, private msg: MatSnackBar,
    public dialog: MatDialog, private dateAdapter: DateAdapter<Date>) {
    this.form = new FormGroup({
      'nombre' : new FormControl('', [Validators.required]),
      'fechaNacimiento' : new FormControl('', [Validators.required]),
      'pais' : new FormControl('', [Validators.required]),
    });
    this.dateAdapter.setLocale('es-ES')
   }

   
   getPaises(){
    this.autores_service.getCountries().subscribe(data => {
      this.countries = data;

      this.paisesOptions = this.form.controls["pais"].valueChanges.pipe(
        startWith(''),
        map(value => this._filter_paises(value || '')),
      );

    });
   }


   private _filter_paises(value: string): AutorEntity[] {
    const filterValue = value.toLowerCase();

    return this.countries.filter(option => 
      option.name.common.toLowerCase().includes(filterValue)
      );
  }


  ngOnInit(): void {
    this.getPaises();
  }

  crear(){
    let datos_autores : AutorEntity = new AutorEntity();
    datos_autores = this.form.value;

    this.autores_service.save(datos_autores).subscribe(
      {
        next:(response) => {
          this.msg.open('¡¡ Autor creado con éxito!!', 'Ok',{
            duration: 4000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['success_dialog']
          });
          this.autores_service .setRefresh(true);
          this.dialog.closeAll();
        },
        error:(err) =>{
          this.msg.open('¡¡ Algo salió mal !!', 'Ok',{
            duration: 4000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['error_dialog']
          })
        }
      }
    );
  }

}
